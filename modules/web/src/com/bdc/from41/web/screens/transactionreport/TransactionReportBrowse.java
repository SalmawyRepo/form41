package com.bdc.from41.web.screens.transactionreport;

import com.bdc.from41.entity.FinancialYear;
import com.bdc.from41.entity.MyQuarter__;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.TransactionReport;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;

@UiController("TransactionReport.browse")
@UiDescriptor("transaction-report-browse.xml")
@LookupComponent("transactionReportsTable")
@LoadDataBeforeShow
public class TransactionReportBrowse extends StandardLookup<TransactionReport> {
    @Inject
    private CollectionContainer<MyQuarter__> quartersDc;


    @Inject
    private CollectionContainer<TransactionReport> transactionReportsDc;
    @Inject
    private DataManager dataManager;
    @Inject
    private Logger log;

    @Subscribe
    public void onInit(InitEvent event) {
        transactionReportsDc.getMutableItems().clear();
    }


    @Subscribe("quarterDDL")
    public void onQuarterDDLValueChange(HasValue.ValueChangeEvent<MyQuarter__> event) {
log.info("String.valueOf( event.getPrevValue().getId()) =>"+ event.getValue().getId());
        transactionReportsDc.getMutableItems().clear();
        FinancialYear fn=dataManager.load(FinancialYear.class).query("e.active='1'").one();
        List transactions= dataManager.load(TransactionReport.class).query("e.quarterNum=?1 and e.finYearId=?2 ",
                           String.valueOf( event.getValue().getId()),fn.getId()).list();
        transactionReportsDc.getMutableItems().addAll(transactions);






    }



}