package com.bdc.from41.web.screens.financialyear;

import com.haulmont.cuba.gui.components.actions.EditAction;
import com.haulmont.cuba.gui.data.CollectionDatasource;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.DataComponents;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.FinancialYear;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

@UiController("FinancialYear.browse")
@UiDescriptor("financial-year-browse.xml")
@LookupComponent("financialYearsTable")
@LoadDataBeforeShow
public class FinancialYearBrowse extends StandardLookup<FinancialYear> {

}