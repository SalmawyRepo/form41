package com.bdc.from41.web.screens.transaction;

import com.bdc.from41.core.enums.TransactionStatusEnum;
import com.bdc.from41.entity.*;
import com.bdc.from41.service.ITransactionService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.global.UserSession;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;

@UiController("from41_Transactions.browse")
@UiDescriptor("transactions-browse.xml")
@LookupComponent("transactionsTable")
//@LoadDataBeforeShow
public class TransactionsBrowse extends StandardLookup<Transaction> {
    @Inject
    private CollectionContainer<Transaction> savedTransactionsDc;
    @Inject
    private Button validateBtn;
    @Inject
    private Logger log;
    @Inject
    private DataManager dataManager;
    @Inject
    private ITransactionService transactionService;
    @Inject
    private LookupField<MyQuarter__> quarterDDL;
    @Inject
    private UserSession userSession;
    @Inject
    private Notifications notifications;
    @Inject
    private MessageBundle messageBundle;
    @Inject
    private LookupField vQuarterDDL;
    @Inject
    private CollectionContainer<Transaction> validatedTransactionsDc;

    @Subscribe
    public void onInit(InitEvent event) {

        List quarters=dataManager.load(MyQuarter__.class).list();
        quarterDDL.clear();
        quarterDDL.setOptionsList(quarters);


        vQuarterDDL.clear();
        vQuarterDDL.setOptionsList(quarters);

    }


    @Subscribe("vQuarterDDL")
    public void onVQuarterDDLValueChange(HasValue.ValueChangeEvent<MyQuarter__> event) {
        if(event.getValue()!=null) {
            List <Transaction> transactions=   getTransactions(event.getValue().getId(),TransactionStatusEnum.VALIDATED.getId());
            validatedTransactionsDc.getMutableItems().addAll(transactions);
        }
    }

    @Subscribe("quarterDDL")
    public void onQuarterDDLValueChange(HasValue.ValueChangeEvent<MyQuarter__> event) {
        if(event.getValue()!=null) {
           List <Transaction> transactions= getTransactions(event.getValue().getId(),TransactionStatusEnum.SAVED.getId());
            savedTransactionsDc.getMutableItems().addAll(transactions);

        }
        }


private List<Transaction> getTransactions(int quarterId,int status){
    savedTransactionsDc.getMutableItems().clear();
         validateBtn.setEnabled(true);

        FinancialYear fn = dataManager.load(FinancialYear.class).query(" e.active='1'").one();
        List <Transaction> transactions = transactionService.getTransaction(fn.getId(),0,quarterId,status);
        return transactions;
    }

    @Subscribe("validateBtn")
    public void onValidateBtnClick(Button.ClickEvent event) {

        FinancialYear year=transactionService.getActiveFinancialYears();
        int quarterId =this.quarterDDL.getValue().getId();

        String branchCode=   ((TaxesUser)userSession.getUser()).getBranch().getCode();

       int result= this.transactionService.validateQuartertransactions(year.getId(),branchCode,quarterId);

      /*
       BigDecimal g=transactionService.getQuarterGLBlance(year.getId(),branchCode,quarterId);
        BigDecimal t=transactionService.getQuarterTransactionsBlance(year.getId(),branchCode,quarterId);
       */


        switch (result){
    case 1:  notifications.create().withType(Notifications.NotificationType.ERROR).withCaption(messageBundle.getMessage("msg.TBGTGLB")).show();break;

    case -1:  notifications.create().withType(Notifications.NotificationType.ERROR).withCaption(messageBundle.getMessage("msg.TBLTGLB")).show();break;
    case 0:
         List <Transaction> transactions= getTransactions(quarterDDL.getValue().getId(),TransactionStatusEnum.SAVED.getId());
        savedTransactionsDc.getMutableItems().addAll(transactions);
    notifications.create().withType(Notifications.NotificationType.HUMANIZED).withCaption(messageBundle.getMessage("msg.transaction.validate.done")).show();

        break;


}


    }
}