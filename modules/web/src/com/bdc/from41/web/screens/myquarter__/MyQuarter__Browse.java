package com.bdc.from41.web.screens.myquarter__;

import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.MyQuarter__;

@UiController("from41_MyQuarter__.browse")
@UiDescriptor("my-quarter__-browse.xml")
@LookupComponent("myQuarter__sTable")
@LoadDataBeforeShow
public class MyQuarter__Browse extends StandardLookup<MyQuarter__> {
}