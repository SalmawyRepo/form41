package com.bdc.from41.web.screens.glblance;

import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.GLBlance;

@UiController("GLBlance.browse")
@UiDescriptor("gl-blance-browse.xml")
@LookupComponent("gLBlancesTable")
@LoadDataBeforeShow
public class GLBlanceBrowse extends StandardLookup<GLBlance> {
}