package com.bdc.from41.web.screens.transactionstatus;

import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.TransactionStatus;

@UiController("from41_TransactionStatus.browse")
@UiDescriptor("transaction-status-browse.xml")
@LookupComponent("transactionStatusesTable")
@LoadDataBeforeShow
public class TransactionStatusBrowse extends StandardLookup<TransactionStatus> {
}