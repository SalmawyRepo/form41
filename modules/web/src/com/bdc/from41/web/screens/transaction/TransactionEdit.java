package com.bdc.from41.web.screens.transaction;

import com.bdc.from41.core.enums.TransactionTypeEnum;
import com.bdc.from41.entity.*;
import com.bdc.from41.service.ITransactionService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.global.UserSession;
import com.haulmont.cuba.web.security.LoginProvider;
import org.slf4j.Logger;
 import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.*;

@UiController("Transaction.edit")
@UiDescriptor("transaction-edit.xml")
@EditedEntityContainer("transactionDc")
@LoadDataBeforeShow
public class TransactionEdit extends StandardEditor<Transaction> {
      @Inject
    private LoginProvider loginProvider;
    @Inject
    private TextField<String> taxesCommission;
    @Inject
    private TextField<Double> netAmountField;
    @Inject
    private ITransactionService transactionService;
    @Inject
    private Notifications notifications;
    @Inject
    private Messages messages;

    @Inject
    private Logger log;
    @Inject
    private TextField<Double> grossAmountField;
    @Inject
    private TextField<String> totalTaxes;
    @Inject
    private DataManager dataManager;
    @Inject
    private LookupField<FinancialYear> financialYearField;
    @Inject
    private LookupField<Branch> branchCodeField;
    @Inject
    private UserSession userSession;
    @Inject
    private CollectionContainer<MyQuarter__> quartersDc;
    @Inject
    private Button saveBtn;
    @Inject
    private LookupField<MyQuarter__> quarterField;


    @Subscribe("transactionDateField")
    public void onTransactionDateFieldValueChange(HasValue.ValueChangeEvent<Date> event) {
       Date date= event.getValue();

        SimpleDateFormat sdf = new SimpleDateFormat("MM");

        if (date.compareTo(new Date()) == 1) {
           notifications.create().withCaption("date must be less or equal to today").withType(Notifications.NotificationType.ERROR).show();
            event.getComponent().setValue(new Date());
        }




    }

    @Subscribe("quarterField")
    public void onQuarterFieldValueChange(HasValue.ValueChangeEvent<MyQuarter__> event) {
        validateQurter();



     }



private boolean validateQurter(){
    List <MyQuarter__> quarters=this.transactionService.getSuggestedQuarters();

    boolean quarterExist=false ;
    for (MyQuarter__ q:quarters){

        if(q.getId()==quarterField.getValue().getId())
            quarterExist=true;
        break;
    }
    if (!quarterExist){

        notifications.create().withType(Notifications.NotificationType.ERROR).withCaption(messages.getMainMessage("msg.should.sggestedQuarters")+quarters.toString()).show();
    }
        return quarterExist;
}
    private boolean validateGrossAmount(){

        Optional<Double> grossAmountValue=Optional.of( grossAmountField.getValue());
        if(grossAmountValue.isPresent()){




        }else{


        }



        List <MyQuarter__> quarters=this.transactionService.getSuggestedQuarters();

        boolean quarterExist=false ;
        for (MyQuarter__ q:quarters){

            if(q.getId()==quarterField.getValue().getId())
                quarterExist=true;
            break;
        }
        if (!quarterExist){

            notifications.create().withType(Notifications.NotificationType.ERROR).withCaption(messages.getMainMessage("msg.should.sggestedQuarters")+quarters.toString()).show();
        }
        return quarterExist;
    }


    @Subscribe("transactionTypeField")
    public void onTransactionTypeFieldValueChange(HasValue.ValueChangeEvent<TransactionType> event) {
    log.info( event.getValue().getName());


        String s= event.getValue().getCommission().toString();
         taxesCommission.setValue( s);
    }


    @Subscribe
    public void onInitEntity(InitEntityEvent<Transaction> event) {
        TaxesUser user= (TaxesUser) userSession.getUser();
        if(user.getBranch()==null ) {
             event.getEntity().setBranch(user.getBranch());
        }
    }


    @Subscribe
    public void onInit(InitEvent event) {
       TaxesUser user= (TaxesUser) userSession.getUser();

        List<Branch>branches=new ArrayList<>(Arrays.asList(user.getBranch()));
        if(user.getBranch()==null )
            branches= dataManager.load(Branch.class).list();

         branchCodeField.clear();
         branchCodeField.setOptionsList(branches);
         branchCodeField.setValue(user.getBranch());



Transaction t=new Transaction();
t.setBranch(user.getBranch());
this.setEntityToEdit(t);

 //===========================================================================================================

        List<FinancialYear>fns= dataManager.load(FinancialYear.class).query("e.active='1'").list();
   financialYearField.clear();
        financialYearField.setOptionsList(fns);
//===========================================================================================================
        grossAmountField. addValidator(value -> {

            String msg=messages.getMainMessage("err.grossAmount");
            log.info("validator");
            if (value <300)
                throw new ValidationException(msg);
        });


     }



    @Subscribe("grossAmountField")
    public void onGrossAmountFieldValueChange(HasValue.ValueChangeEvent<Double> event) {
        log.info("onGrossAmountFieldValueChange");

        if (event.getValue() <300) {

            return;
        }

        netAmountField.setValue(event.getValue());
        calculateTotalTaxes();
    }

    @Subscribe("taxesCommission")
    public void onTaxesCommissionValueChange(HasValue.ValueChangeEvent<String> event) {
        calculateTotalTaxes();
    }

private void calculateTotalTaxes(){

    double commissionValue =(taxesCommission.getValue().isEmpty())?0.0:Double.parseDouble(taxesCommission.getRawValue());
    double grossAmountValue= (grossAmountField.getValue()!=null)?grossAmountField.getValue():0.0;

    this.totalTaxes.setValue(String.valueOf(commissionValue*grossAmountValue));

}

}