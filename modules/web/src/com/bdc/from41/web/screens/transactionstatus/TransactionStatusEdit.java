package com.bdc.from41.web.screens.transactionstatus;

import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.TransactionStatus;

@UiController("from41_TransactionStatus.edit")
@UiDescriptor("transaction-status-edit.xml")
@EditedEntityContainer("transactionStatusDc")
@LoadDataBeforeShow
public class TransactionStatusEdit extends StandardEditor<TransactionStatus> {
}