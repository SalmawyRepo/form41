package com.bdc.from41.core.enums;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum TransactionStatusEnum implements EnumClass<Integer> {

    VALIDATED(2),
    SAVED(1);

    private Integer id;

    TransactionStatusEnum(Integer value) {
        this.id = value;
    }

    public Integer getId() {
        return id;
    }

    @Nullable
    public static TransactionStatusEnum fromId(Integer id) {
        for (TransactionStatusEnum at : TransactionStatusEnum.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}