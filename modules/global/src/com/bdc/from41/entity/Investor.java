package com.bdc.from41.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;
import com.haulmont.cuba.core.entity.HasUuid;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.UUID;

@Table(name = "INVESTORS")
@Entity(name = "from41_Investor")
@NamePattern("%s|name")
public class Investor extends BaseIntegerIdEntity implements HasUuid {
    private static final long serialVersionUID = -4667068120112912722L;
    @Column(name = "UUID")
    private UUID uuid;



    @Column(name = "NAME")
    private String name;

    @Column(name = "PHONE", length = 14)
    private String phone;

    @Column(name = "ADDRESS", length = 1024)
    private String address;

    @Column(name = "NATIONAL_ID", length = 14)
    private String nationalId;

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Override
    public UUID getUuid() {
        return uuid;
    }

    @Override
    public void setUuid(UUID uuid) {
        this.uuid=uuid;
    }
}