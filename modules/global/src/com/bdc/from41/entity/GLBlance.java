package com.bdc.from41.entity;

import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Table(name = "GL_BALNCES")
@Entity(name = "GLBlance")
public class GLBlance extends BaseIntegerIdEntity {
    private static final long serialVersionUID = -4098528489599379454L;


    @Column(name = "FIN_YEAR", nullable = false)
    @NotNull
    private String finYear;

    @Column(name = "PERIOD_CODE", nullable = false)
    @NotNull
    private String periodCode;

    @Column(name = "BRANCH_CODE", nullable = false)
    @NotNull
    private String branchCode;

    @Column(name = "CATEGORY")
    private String category;

    @Column(name = "PARENT_GL", nullable = false)
    @NotNull
    private String parentGl;

    @Column(name = "GL_CODE", nullable = false)
    @NotNull
    private String glCode;

    @Column(name = "LEAF", nullable = false)
    @NotNull
    private String leaf;

    @Column(name = "CCY_CODE", nullable = false)
    @NotNull
    private String ccyCode;

    @Column(name = "F_C", nullable = false)
    @NotNull
    private BigDecimal fC;

    @Column(name = "EQUIVALENT_BALANCE", nullable = false)
    @NotNull
    private BigDecimal equivalentBalance;

    @Column(name = "EGP_BALANCE", nullable = false)
    @NotNull
    private BigDecimal egpBalance;

    @Column(name = "BALANCE", nullable = false)
    @NotNull
    private BigDecimal balance;

    @Column(name = "GL_DESC", nullable = false, length = 1024)
    @NotNull
    private String glDesc;

    @Column(name = "BRANCH_NAME", nullable = false, length = 512)
    @NotNull
    private String branchName;













    public String getBranchName() {
        return branchName;
    }

    public String getGlDesc() {
        return glDesc;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public BigDecimal getEgpBalance() {
        return egpBalance;
    }

    public BigDecimal getEquivalentBalance() {
        return equivalentBalance;
    }

    public BigDecimal getfC() {
        return fC;
    }

    public String getCcyCode() {
        return ccyCode;
    }

    public String getLeaf() {
        return leaf;
    }

    public String getGlCode() {
        return glCode;
    }

    public String getParentGl() {
        return parentGl;
    }

    public String getCategory() {
        return category;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public String getPeriodCode() {
        return periodCode;
    }

    public String getFinYear() {
        return finYear;
    }

}