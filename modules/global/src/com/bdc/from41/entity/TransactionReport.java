package com.bdc.from41.entity;

import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;

import javax.persistence.*;
import java.util.Date;

@Table(name = "Transaction_Report")
@Entity(name = "TransactionReport")
public class TransactionReport extends BaseIntegerIdEntity {

    @Column(name = "QUARTER", nullable = false)

    private String quarterNum;

    @Column(name = "FIN_YEAR_ID")
    private Integer finYearId;

    @Column(name = "COMMISSION")
    private Double commission;

    @Column(name = "TOTAL_TAX")
    private Double totalTax;

    @Column(name = "INVESTOR_NAME")
    private String investorName;

    @Column(name = "INVESTOR_ADDRESS")
    private String investorAddress;

    @Column(name = "NATIONAL_ID")
    private String nationalId;

    @Column(name = "CURENCY")
    private Integer currency;

    @Column(name = "FIN_YEAR", nullable = false)
    private String financialYear;

    @Temporal(TemporalType.DATE)
    @Column(name = "TRANSACTION_DATE")
    private Date transactionDate;

    @Column(name = "REGISTERATION_NUMBER")
    private String registertationNumber;

    @Column(name = "FILE_NO")
    private Character fileNo;

    @Column(name = "MASSION_CODE")
    private Character massionCode;

    @Column(name = "CODE")
    private String code;

    @Column(name = "GROSS_AMOUNT")
    private Double grossAmount;

    @Column(name = "DISCOUNT_TYPE")
    private Double discountType;

    @Column(name = "NET_AMOUNT")
    private Double netAmount;

    public Integer getFinYearId() {
        return finYearId;
    }

    public void setFinYearId(Integer finYearId) {
        this.finYearId = finYearId;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Character getFileNo() {
        return fileNo;
    }

    public Integer getCurrency() {
        return currency;
    }

    public String getNationalId() {
        return nationalId;
    }

    public String getInvestorAddress() {
        return investorAddress;
    }

    public String getInvestorName() {
        return investorName;
    }

    public Double getTotalTax() {
        return totalTax;
    }

    public Double getCommission() {
        return commission;
    }

    public Double getNetAmount() {
        return netAmount;
    }

    public Double getDiscountType() {
        return discountType;
    }

    public Double getGrossAmount() {
        return grossAmount;
    }

    public String getCode() {
        return code;
    }

    public Character getMassionCode() {
        return massionCode;
    }

    public String getRegistertationNumber() {
        return registertationNumber;
    }

    public String getFinancialYear() {
        return financialYear;
    }

    public String getQuarterNum() {
        return quarterNum;
    }
}