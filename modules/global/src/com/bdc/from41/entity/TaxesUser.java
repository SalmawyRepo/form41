package com.bdc.from41.entity;

import com.haulmont.cuba.core.entity.annotation.Extends;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;
import com.haulmont.cuba.security.entity.User;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity(name = "TaxesUser")
@Extends(User.class)
public class TaxesUser extends User {


    @Lookup(type = LookupType.SCREEN, actions = "lookup")
    @ManyToOne
    @JoinColumn(name = "BRANCH_CODE")
    private Branch branch;

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }
}