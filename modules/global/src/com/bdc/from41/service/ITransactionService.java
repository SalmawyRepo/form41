package com.bdc.from41.service;

import com.bdc.from41.entity.FinancialYear;

import java.math.BigDecimal;
import java.util.List;

public interface ITransactionService {
    String NAME = "TransactionService";




    List getSuggestedQuarters();


    FinancialYear getActiveFinancialYears();

    void restFinancialYears(int id);

    List getTransaction(int finYearId, int branchCodeId, int quarterId, int statusId);

     int validateQuartertransactions(int yearId, String branchCode, int quarterId);

      BigDecimal getQuarterTransactionsBlance(int yearId, String branchCode, int quarterId);

    void changeTransactionTovalidatedStatus(int yearId, String branchCode, int quarterId);

      BigDecimal getQuarterGLBlance(int yearId, String branchCode, int quarterId);
}