-- begin TRANSACTIONS
create table TRANSACTIONS (
    ID number(10),
    UUID varchar2(32),
    --
    BRANCH_CODE_ID number(10),
    FINANCIAL_YEAR_ID number(10) not null,
    QUARTER_ID number(10) not null,
    DISCOUNT_TYPE float,
    INVESTOR_ID number(10),
    TRANSACTION_TYPE_ID number(10) not null,
    TRANSACTION_DATE date not null,
    CURENCY number(10),
    GROSS_AMOUNT float not null,
    NET_AMOUNT float,
    REGISTERATION_NUMBER varchar(255),
    STATUS_ID number(10),
    --
    primary key (ID)
)^
-- end TRANSACTIONS
-- begin FINANCIAL_YEARS
create table FINANCIAL_YEARS (
    ID number(10),
    UUID varchar2(32),
    --
    FIN_YEAR_CODE varchar2(255 char),
    NAME varchar2(255 char),
    ACTIVE char default ('0'),
    FROM_DATE date,
    TO_DATE date,
    --
    primary key (ID)
)^
-- end FINANCIAL_YEARS
-- begin TRANSACTION_STATUS
create table TRANSACTION_STATUS (
    ID number(10),
    --
    NAME varchar2(255 char),
    NAME_AR varchar2(255 char),
    --
    primary key (ID)
)^
-- end TRANSACTION_STATUS
-- begin TRANSACTION_TYPE
create table TRANSACTION_TYPE (
    ID number(10),
    --
    CODE varchar2(255 char),
    MINMUM_AMOUNT float,
    NAME varchar2(255 char),
    COMMISSION float,
    --
    primary key (ID)
)^
-- end TRANSACTION_TYPE
-- begin QUARTERS
create table QUARTERS (
    ID number(10),
    --
    NAME varchar2(255 char),
    FROM_MONTH varchar2(255 char),
    --
    primary key (ID)
)^
-- end QUARTERS
-- begin GL_BALNCES
create table GL_BALNCES (
    ID number(10),
    --
    FIN_YEAR varchar2(255 char) not null,
    PERIOD_CODE varchar2(255 char) not null,
    BRANCH_CODE varchar2(255 char) not null,
    CATEGORY varchar2(255 char),
    PARENT_GL varchar2(255 char) not null,
    GL_CODE varchar2(255 char) not null,
    LEAF varchar2(255 char) not null,
    CCY_CODE varchar2(255 char) not null,
    F_C number(19, 2) not null,
    EQUIVALENT_BALANCE number(19, 2) not null,
    EGP_BALANCE number(19, 2) not null,
    BALANCE number(19, 2) not null,
    GL_DESC varchar2(1024 char) not null,
    BRANCH_NAME varchar2(512 char) not null,
    --
    primary key (ID)
)^
-- end GL_BALNCES
-- begin INVESTORS
create table INVESTORS (
    ID number(10),
    UUID varchar2(32),
    --
    NAME varchar2(255 char),
    PHONE varchar2(14 char),
    ADDRESS varchar2(1024 char),
    NATIONAL_ID varchar2(14 char),
    --
    primary key (ID)
)^
-- end INVESTORS
-- begin BRANCHES
create table BRANCHES (
    ID number(10),
    --
    BRANCH_CODE varchar2(15 char) not null,
    BRANCH_LOCATION varchar2(1024 char),
    BRANCH_TYPE varchar2(255 char),
    BRANCH_ADDRESS varchar2(1024 char),
    BRANCH_ADDRESS2 varchar2(1024 char),
    BRANCH_ADDRESS3 varchar2(1024 char),
    BRANCH_STATUS varchar2(255 char),
    BRANCH_NAME varchar2(1024 char),
    BRANCH_COUNTRY varchar2(255 char),
    --
    primary key (ID)
)^
-- end BRANCHES
-- begin SEC_USER
-- alter table SEC_USER add ( BRANCH_CODE number(10) ) ^
-- update SEC_USER set BRANCH_CODE = <default_value> ^
-- alter table SEC_USER modify BRANCH_CODE number(10) not null ^
alter table SEC_USER add ( BRANCH_CODE number(10) not null ) ^
alter table SEC_USER add ( DTYPE varchar2(31 char) ) ^
update SEC_USER set DTYPE = 'TaxesUser' where DTYPE is null ^
-- end SEC_USER
